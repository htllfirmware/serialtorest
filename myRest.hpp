#ifndef MY_REST_H
#define MY_REST_H
  #ifndef UA
    #define UA "Embeded 1.0.9"
  #endif
  #include <Arduino.h>
  #include <WiFiClient.h>
  #include <WiFiClientSecure.h>
  #include <ArduinoJson.h>
  #include <ESP8266WiFi.h>
  #include "util.hpp"
  /*
  @params
    String jsonConfig;
    {
    "ssl": false,
    "host": "192.168.1.162",
    "port": 10443,
    "hostName": "my.host",
    "method": "POST",
    "path": "/PATH",
    "headers": ["AUTH: 31337", "X-X-X: true"],
    "body": "{\"a\": true}"
    }
    all fields are mandatory
  @returns
    String jsonConfig;
    {
      "call": "rest"
      "error": 0 ;  0 ok
                ;  -1 connection error
                ;  -2 wrong JSON argument
                ;  -3 wifi is not connected
      "data": "" ; REST result
    }
   */
  JsonObject & sendHttpRequest(JsonObject &  jsonConfig);
#endif
