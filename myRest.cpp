
#include "myRest.hpp"
#define HEADERS_COUNT(a) \
  sizeof(a) / sizeof(header*)

bool checkRestConfig(JsonObject & config ) {
   if (config.containsKey( F("host")) == false) {
     return false;
   }
   if (config.containsKey( F("hostName")) == false) {
     return false;
   }
   if (config.containsKey( F("path")) == false) {
     return false;
   }
   if (config.containsKey( F("port")) == false) {
     return false;
   }
   if (config.containsKey( F("headers")) == false) {
     return false;
   }
   if (config.containsKey( F("body")) == false) {
     return false;
   }
   if (config.containsKey( F("method")) == false) {
     return false;
   }
   return true;
}

String makeRequest(String path, String hostName,  JsonArray  & headers,  String  body, String method = "GET"){
  String _headers = "";
   for(int i = 0; i < headers.size(); i++){
     _headers += headers[i].as<String>() + "\r\n";
   }
   return (method + " " + path +  F(" HTTP/1.1\r\n") +
                 F("Host: ") + hostName +  ("\r\n") +
                 F("User-Agent: ") + UA + ("\r\n") +
                 F("Content-type: application/json\r\n") +
                 _headers +
                 F("Content-Length: ") + body.length() + "\r\n\r\n" +
                 body + "\r\n\r\n"
              );
}

JsonObject & sendHttpRequest(JsonObject &   config){
  int error = 0;
  String data = "";
  if(checkRestConfig(config) == false){
    error = -2;
    return buildResult(F("rest"), error, data);
  }
  if(WiFi.status() != WL_CONNECTED) {
    error = -3;
    return buildResult(F("rest"), error, data);
  }
  bool ssl = config[F("ssl")].as<bool>();
  String Req = makeRequest(config[F("path")].as<String>(), config[F("hostName")].as<String>(), config[F("headers")].as<JsonArray&>(),  config[F("body")].as<String>(),  config[F("method")].as<String>());
  if(ssl){
    WiFiClientSecure client;
    if (!client.connect((config[F("host")].as<const char*>()), config[F("port")].as<int>())) {
      error = -1;
      return buildResult(F("rest"), error, data);
    }
    client.print(Req);
    while (client.connected()) {
      data += String(client.readStringUntil('\n')) + "\n";
    }
    return buildResult(F("rest"), error, data);
  } else {
    WiFiClient client;
    if (!client.connect((config[F("host")].as<const char*>()), config[F("port")].as<int>())) {
      error = -1;
      return buildResult(F("rest"), error, data);
    }
    client.print(Req);
    while (client.connected()) {
      data +=  String(client.readStringUntil('\n')) + "\n";
    }
    return buildResult(F("rest"), error, data);
  }
}
