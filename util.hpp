#ifndef UTIL_H
#define UTIL_H
  #ifndef LED
    #define LED 2 //led pin ( 2 = built-in LED)
  #endif
  #ifndef INVERTED
    #define INVERTED true // invert HIGH/LOW for the LED
  #endif

  #include <Arduino.h>
  #include <ArduinoJson.h>

  void initLed();
  /*
  @params
    int state LED state
  */
  void led(bool state);

  /*
  @params
    int n blink count
  */
  void longBlink(int n=1);

  /*
  @params
    int n blink count
  */
  void shortBlink(int n=1);

  /*
  @params
    int error error code
    String data data field

  @returns
   json like
   {"error": 0, "data": "some data"}
   */
  // };
  JsonObject & buildResult(String call, int error, String data = "");
#endif
