#ifndef MY_WFI_H
#define MY_WFI_H
  #define WRONG_CRED  "WrongAp124122142"
  #include <Arduino.h>
  #include <ArduinoJson.h>
  #include <ESP8266WiFi.h>
  #include "util.hpp"

  /*
  @params
    String jsonConfig;
    {
    "essid": "ESSID",
    "password": "p@$$w0rd"
    }
    all fields are mandatory

  @returns
    String jsonConfig;
    {
      "call": "connect"
      "error": 0 ;  0 ok
                ;  -2 wrong JSON argument
                ;  -10 connection error
    }
   */
  JsonObject & connectWifi(JsonObject & config);
  void initWifi();
#endif
