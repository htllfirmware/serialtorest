

// #define REST_CONFIG "{ \
//   \"ssl\": false, \
//   \"host\": \"192.168.1.162\", \
//   \"port\": 10443, \
//   \"hostName\": \"my.host\", \
//   \"method\": \"POST\", \
//   \"path\": \"/PATH\", \
//   \"headers\": [ \
//     \"AUTH: 31337\", \
//     \"X-X-X: true\" \
//   ], \
//   \"body\": \"123\"\
// }"

#include <ArduinoJson.h>
#include "util.hpp"
#include "myRest.hpp"
#include "myWifi.hpp"

String inputString = "";

void setup() {
  Serial.begin(115200);
  initLed();
  while (!Serial) continue;
  initWifi();
  Serial.println();
}

void wifiIndicator(){
  if (WiFi.status() == WL_CONNECTED) {
    longBlink();
  } else {
    shortBlink();
  }
}

void loop() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n') {
      procCmd(inputString);
      inputString = "";
    }
  }
  wifiIndicator();
}


void printResult(JsonObject & result){
  result.printTo(Serial);
  Serial.println();
}

void procCmd(String cmd){
  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject & jsonCmd = jsonBuffer.parseObject(cmd);
  if (jsonCmd.success() == false) {
    printResult(buildResult("", -2, ""));
    return;
  }
  if (jsonCmd.containsKey(F("call")) == false) {
    printResult(buildResult("", -2, ""));
    return;
  }
  if (jsonCmd[ F("call")].as<String>() == F("rest")){
    printResult(sendHttpRequest(jsonCmd));
  }
  if (jsonCmd[ F("call")].as<String>() == F("connect")){
    printResult(connectWifi(jsonCmd));
  }
}
