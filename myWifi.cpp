
#include "myWifi.hpp"

bool checkWifiConfig(JsonObject & config ) {
   if (config.containsKey( F("essid")) == false) {
     return false;
   }
   if (config.containsKey( F("password")) == false) {
     return false;
   }
   return true;
}

void initWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(WRONG_CRED, WRONG_CRED);
}

bool _connectWifi(const char * essid, const char * password, int timeout = 10){
  WiFi.begin(essid, password);
  for (int i =0; i < timeout && WiFi.status() != WL_CONNECTED; i++) {
    delay(500);
    shortBlink(10);
  }
  return  WiFi.status() == WL_CONNECTED;
}

JsonObject & connectWifi(JsonObject & config){
  int error = 0;
  if(checkWifiConfig(config) == false){
    error = -2;
    return buildResult( F("connect"), error);
  }
  if(_connectWifi(config[ F("essid")].as<const char *>(), config["password"].as<const char *>()) == false){
    error = -10;
  }
  return buildResult( F("connect"), error);

}
